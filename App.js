import React, {Component} from 'react';
import {StyleSheet, View, Text,TouchableOpacity} from 'react-native';



export default class App extends Component {
  state = {content: false};

  componentHideAndShow = () => {
    this.setState((previousState) => ({content: !previousState.content}));
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.textContainer}>
          <Text style={styles.baseText}>Zadanie 2</Text>
        </View>
        <TouchableOpacity style={styles.button} onPress={this.componentHideAndShow}>
          <Text>{this.state.content ? "UKRYJ" : "POKAŻ"}</Text>
        </TouchableOpacity>
        <View style={styles.textContainer}>
        {this.state.content ? (
          <Text style={styles.baseText}>Nazywam się <Text style={styles.boldText}>Gabriela Jasnosz </Text>
          </Text>
        ) : null}
        </View>
      </View>


    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
    button: {
      alignSelf: 'center',
      width: 100,
      alignItems: 'center',
      backgroundColor: '#9940a5',
      padding: 10,
    },
    textContainer: {
      alignItems: 'center',
      padding: 10,
    },
    baseText: {
      fontSize: 18,
    },
    boldText: {
      fontWeight: 'bold',
    },
  });
